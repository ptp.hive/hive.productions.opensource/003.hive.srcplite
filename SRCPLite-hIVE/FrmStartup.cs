﻿using System;
using System.Windows.Forms;
//
using SRCPLite_hIVE.Models_CS;
using ECP.PersianMessageBox;
using System.IO;

namespace SRCPLite_hIVE
{
    public partial class FrmStartup : Form
    {
        public FrmStartup()
        {
            InitializeComponent();
        }

        #region ConnectionModels
        //----------

        private FileManager _FileManager = new FileManager();
        private SRC_DBs _SRCDBs = new SRC_DBs();

        private TryCatch _TryCatch = new TryCatch();
        private string MesExpErrorForms = string.Empty;

        //----------
        #endregion ConnectionModels

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        #region Form Main
        //----------

        private void FrmStartup_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (PersianMessageBox.Show("آیا می‌خواهید خارج شوید؟   \n",
                                       "خروج",
                                       PersianMessageBox.Buttons.YesNo,
                                       PersianMessageBox.Icon.Question,
                                       PersianMessageBox.DefaultButton.Button2) != DialogResult.Yes)
            {
                e.Cancel = true;
            }
        }

        private void btnAboutUS_Click(object sender, EventArgs e)
        {
            try
            {
                Form _FrmInfo = new FrmInfo();
                MPG_WinShutDowm.ShowShutForm ssf = new MPG_WinShutDowm.ShowShutForm(new FrmInfo());
                _FrmInfo.Close();
            }
            catch { }
        }

        //----------
        #endregion Form Main

        #region FunctionType
        //----------

        private bool LoadFunctionType()
        {
            try
            {
                rdbDbPassSet.Checked = false;
                rdbDbPassRemove.Checked = false;
                rdbDbPassChange.Checked = false;

                grbInputDbFile.Visible = false;
                grbDbPassword.Visible = false;
                grbNewDbPassword.Visible = false;
                tlpRunCancleFunction.Visible = false;
            }
            catch { return false; }

            return true;
        }

        private void rdbDbPassSet_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                grbInputDbFile.Visible = true;

                grbDbPassword.Visible = true;
                grbDbPassword.Text = "پسورد مورد نظر";

                grbNewDbPassword.Visible = false;
                tlpRunCancleFunction.Visible = true;
            }
            catch { }
        }

        private void rdbDbPassRemove_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                grbInputDbFile.Visible = true;

                grbDbPassword.Visible = true;
                grbDbPassword.Text = "پسورد پایگاه‌داده";

                grbNewDbPassword.Visible = false;
                tlpRunCancleFunction.Visible = true;
            }
            catch { }
        }

        private void rdbDbPassChange_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                grbInputDbFile.Visible = true;

                grbDbPassword.Visible = true;
                grbDbPassword.Text = "پسورد فعلی پایگاه‌داده";

                grbNewDbPassword.Visible = true;
                grbNewDbPassword.Text = "پسورد جدید پایگاه‌داده";

                tlpRunCancleFunction.Visible = true;
            }
            catch { }
        }

        //----------
        #endregion FunctionType

        #region InputDbFile
        //----------

        private bool LoadInputDbFile()
        {
            try
            {
                rdbInDbPathMainSave.Checked = true;
                rdbInDbPathStartupSave.Checked = false;

                txtShowInputDbPath.Text = "";
            }
            catch { return false; }

            return true;
        }

        private bool RefreshOutDbPathSave()
        {
            try
            {
                if (rdbInDbPathMainSave.Checked)
                {
                    _SRCDBs.SetOutDBPath = _SRCDBs.GetInDBPath;
                }
                else if (rdbInDbPathStartupSave.Checked)
                {
                    _SRCDBs.SetOutDBPath = _FileManager.GetDefaultOutDBPath;
                }
            }
            catch { return false; }

            return true;
        }

        private bool RefreshOutDbNameSave()
        {
            try
            {
                string OutputDBName = _SRCDBs.GetInDBName;

                if (rdbDbPassSet.Checked)
                {
                    OutputDBName = "S_" + OutputDBName;
                }
                else if (rdbDbPassRemove.Checked)
                {
                    OutputDBName = "R_" + OutputDBName;
                }
                else if (rdbDbPassChange.Checked)
                {
                    OutputDBName = "C_" + OutputDBName;
                }

                _SRCDBs.SetOutDBName = OutputDBName;
            }
            catch { return false; }

            return true;
        }

        private void btnInDbPathLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog opfInDbPath = new OpenFileDialog();
            opfInDbPath.InitialDirectory = _FileManager.GetDefaultInDBPath;
            opfInDbPath.Title = "Please select an Database file to Inputed.";
            try
            {
                opfInDbPath.Filter = "Database Files (*.db, *.db3, *.sqlite)|*.db; *.db3; *.sqlite" +
                                     "|" + "Any Files (*.*)|*.*";
                if (opfInDbPath.ShowDialog() == DialogResult.OK)
                {
                    _SRCDBs.SetInDBName = Path.GetFileName(opfInDbPath.FileName);
                    _SRCDBs.SetInDBPath = Directory.GetParent(opfInDbPath.FileName).ToString();

                    txtShowInputDbPath.Text = opfInDbPath.FileName;
                }
            }
            catch { }
        }

        //----------
        #endregion InputDbFile

        #region DbPassword
        //----------

        private bool LoadDbPassword()
        {
            try
            {
                txtDbPassword.Text = "";
            }
            catch { return false; }

            return true;
        }

        private void btnDbPassSet_Click(object sender, EventArgs e)
        {
            try
            {
                txtDbPassword.Text = Clipboard.GetText();
            }
            catch { }
        }

        private void btnDbPassDelete_Click(object sender, EventArgs e)
        {
            try
            {
                txtDbPassword.Text = "";
            }
            catch { }
        }

        //----------
        #endregion DbPassword

        #region NewDbPassword
        //----------

        private bool LoadNewDbPassword()
        {
            try
            {
                txtNewDbPassword.Text = "";
            }
            catch { return false; }

            return true;
        }

        private void btnNewDbPassSet_Click(object sender, EventArgs e)
        {
            try
            {
                txtNewDbPassword.Text = Clipboard.GetText();
            }
            catch { }
        }

        private void btnNewDbPassDelete_Click(object sender, EventArgs e)
        {
            try
            {
                txtNewDbPassword.Text = "";
            }
            catch { }
        }

        //----------
        #endregion NewDbPassword

        #region RunCancleFunction
        //----------

        private bool isErrorRunFunction()
        {
            bool isError = false;
            SettingsMain.Default.MessageRunFunction = "قسمت‌های زیر را تکمیل کنید:      \n";

            try
            {
                if (txtShowInputDbPath.Text == null || txtShowInputDbPath.Text == "")
                {
                    SettingsMain.Default.MessageRunFunction += "\n فایل ورودی را انتخاب نمایید. ";
                    isError = true;
                }

                if (txtDbPassword.Text == null || txtDbPassword.Text == "")
                {
                    if (rdbDbPassSet.Checked)
                    {
                        SettingsMain.Default.MessageRunFunction += "\n پسورد مورد نظر را وارد نمایید. ";
                    }
                    else if (rdbDbPassRemove.Checked)
                    {
                        SettingsMain.Default.MessageRunFunction += "\n پسورد پایگاه‌داده را وارد نمایید. ";
                    }
                    else if (rdbDbPassChange.Checked)
                    {
                        SettingsMain.Default.MessageRunFunction += "\n پسورد فعلی پایگاه‌داده را وارد نمایید. ";
                    }

                    isError = true;
                }

                if (rdbDbPassChange.Checked)
                {
                    if (txtNewDbPassword.Text == null || txtNewDbPassword.Text == "")
                    {
                        SettingsMain.Default.MessageRunFunction += "\n پسورد جدید پایگاه‌داده را وارد نمایید. ";
                        isError = true;
                    }
                }
            }
            catch { isError = true; }

            return isError;
        }

        private void btnRunFunction_Click(object sender, EventArgs e)
        {
            try
            {
                if (!isErrorRunFunction())
                {
                    bool Result = false;

                    if (RefreshOutDbPathSave() && RefreshOutDbNameSave())
                    {
                        if (rdbDbPassSet.Checked)
                        {
                            _SRCDBs.SetInDBPass = "";
                            _SRCDBs.SetOutDBPass = txtDbPassword.Text;
                        }
                        else if (rdbDbPassRemove.Checked)
                        {
                            _SRCDBs.SetInDBPass = txtDbPassword.Text;
                            _SRCDBs.SetOutDBPass = "";
                        }
                        else if (rdbDbPassChange.Checked)
                        {
                            _SRCDBs.SetInDBPass = txtDbPassword.Text;
                            _SRCDBs.SetOutDBPass = txtNewDbPassword.Text;
                        }

                        Result = _SRCDBs.SetSRCPLite;
                    }
                }
                else
                {
                    PersianMessageBox.Show(SettingsMain.Default.MessageRunFunction,
                                           "خطا",
                                           PersianMessageBox.Buttons.OK,
                                           PersianMessageBox.Icon.Error,
                                           PersianMessageBox.DefaultButton.Button1);
                }
            }
            catch { }

            _TryCatch.GetCEM_Error(MesExpErrorForms);
            MesExpErrorForms = "";
        }

        private void btnCancleFunction_Click(object sender, EventArgs e)
        {
            try
            {
                LoadFunctionType();
                LoadInputDbFile();
                LoadDbPassword();
                LoadNewDbPassword();

                _TryCatch.GetCEM_Error(MesExpErrorForms);
                MesExpErrorForms = "";
            }
            catch { }
        }

        //----------
        #endregion RunCancleFunction

        //------------
        ////----------////---------------------------------------------------------------------// Begin FormStartup_Load
        //------------

        private void FrmStartup_Load(object sender, EventArgs e)
        {
            string MessageError = string.Empty;

            try
            {
                LoadFunctionType();
                LoadInputDbFile();
                LoadDbPassword();
                LoadNewDbPassword();

                _TryCatch.GetCEM_Error(MesExpErrorForms);
                MesExpErrorForms = "";
            }
            catch
            {
                MessageError = "\nفرآیند بارگیری برنامه با خطا همراه بود.   \n" +
                               "\nمتاسفیم.   \n";

                PersianMessageBox.Show(MessageError,
                                   "خطا",
                                   PersianMessageBox.Buttons.OK,
                                   PersianMessageBox.Icon.Error,
                                   PersianMessageBox.DefaultButton.Button1);
            }
        }

        //------------
        ////----------////---------------------------------------------------------------------// End FormStartup_Load
        //------------
    }
}