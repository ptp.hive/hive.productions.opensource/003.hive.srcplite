﻿using System;
//
using ECP.PersianMessageBox;
using System.Data.SQLite;
using System.IO;

namespace SRCPLite_hIVE.Models_CS
{
    class SRC_DBs : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #region ConnectionModels
        //----------

        private FileManager _FileManager = new FileManager();

        private TryCatch _TryCatch = new TryCatch();
        private string MesExpErrorForms = string.Empty;

        //----------
        #endregion ConnectionModels

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        /// <summary>
        /// Input/Output Parameters DataBases
        /// </summary>
        private static string InputDataBasePath = string.Empty;
        private static string OutputDataBasePath = string.Empty;
        //
        private static string InputDataBaseName = string.Empty;
        private static string OutputDataBaseName = string.Empty;
        //
        private static string InputDataBasePassword = string.Empty;
        private static string OutputDataBasePassword = string.Empty;

        //----------

        /// <summary>
        /// Input String Connection
        /// </summary>
        private string InputStrConnection()
        {
            string _InputStrConnection = ("Data Source=" + InputDataBasePath +
                                          "\\" + InputDataBaseName + "; " +
                                          "Version=3; " +
                                          "Password=" + InputDataBasePassword);
            
            return _InputStrConnection;
        }

        /// <summary>
        /// Output String Connection
        /// </summary>
        private string OutputStrConnection()
        {
            string _OutputStrConnection = ("Data Source=" + OutputDataBasePath +
                                           "\\" + OutputDataBaseName + "; " +
                                           "Version=3; " +
                                           "Password=" + OutputDataBasePassword);

            return _OutputStrConnection;
        }

        //----------

        public bool CreateDirectoryDbPath()
        {
            try
            {
                if (!Directory.Exists(OutputDataBasePath))
                {
                    Directory.CreateDirectory(OutputDataBasePath);
                }
            }
            catch (Exception exp)
            {
                if (!_TryCatch.GetShowFriendlyMessage)
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsSRC_DBs + "۰۱۰۰: " + exp.Message;
                }
                else
                {
                    MesExpErrorForms += _TryCatch.GetMEC_McsSRC_DBs + "۰۱۰۰: " + "خطا در ایجاد پرونده‌ی محل قرارگیری پایگاه‌داده.   ";
                }
            }

            return true;
        }

        private bool SRCPLite()
        {
            bool noError = true;

            try
            {
                SQLiteConnection.ClearAllPools();

                if (CreateDirectoryDbPath())
                {
                    SQLiteConnection InputConnection = new SQLiteConnection(InputStrConnection());
                    SQLiteConnection OutputConnection = new SQLiteConnection(OutputStrConnection());

                    using (InputConnection)
                    {
                        try
                        {
                            InputConnection.Open();
                            OutputConnection.Open();
                            try
                            {
                                InputConnection.BackupDatabase(OutputConnection, "main", "main", -1, null, -1);
                            }
                            catch (Exception exp)
                            {
                                noError = false;

                                if (!_TryCatch.GetShowFriendlyMessage)
                                {
                                    MesExpErrorForms += _TryCatch.GetMEC_McsSRC_DBs + "۰۲۰۱: " + exp.Message;
                                }
                                else
                                {
                                    MesExpErrorForms += _TryCatch.GetMEC_McsSRC_DBs + "۰۲۰۱: " + _TryCatch.GetMEC_ConDB;
                                }
                            }
                            InputConnection.Close();
                            OutputConnection.Close();
                        }
                        catch (Exception exp)
                        {
                            noError = false;

                            if (!_TryCatch.GetShowFriendlyMessage)
                            {
                                MesExpErrorForms += _TryCatch.GetMEC_McsSRC_DBs + "۰۲۰۰: " + exp.Message;
                            }
                            else
                            {
                                MesExpErrorForms += _TryCatch.GetMEC_McsSRC_DBs + "۰۲۰۰: " + _TryCatch.GetMEC_ConDB;
                            }
                        }
                        if (noError)
                        {
                            PersianMessageBox.Show("فرآیند با موفقیت انجام شد.   \n",
                                                   "مدیر",
                                                   PersianMessageBox.Buttons.OK,
                                                   PersianMessageBox.Icon.Information,
                                                   PersianMessageBox.DefaultButton.Button1);
                        }

                        _TryCatch.GetCEM_Error(MesExpErrorForms);
                        MesExpErrorForms = "";
                    }
                }
            }
            catch { noError = false; }

            return noError;
        }

        //----------

        public bool SetSRCPLite
        {
            get { return SRCPLite(); }
        }

        public string GetInDBPath
        {
            get { return InputDataBasePath; }
        }
        public string SetInDBPath
        {
            set { InputDataBasePath = value; }
        }
        public string SetOutDBPath
        {
            set { OutputDataBasePath = value; }
        }

        public string GetInDBName
        {
            get { return InputDataBaseName; }
        }
        public string SetInDBName
        {
            set { InputDataBaseName = value; }
        }
        public string SetOutDBName
        {
            set { OutputDataBaseName = value; }
        }

        public string SetInDBPass
        {
            set { InputDataBasePassword = value; }
        }
        public string SetOutDBPass
        {
            set { OutputDataBasePassword = value; }
        }
    }
}