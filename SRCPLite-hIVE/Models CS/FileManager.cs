﻿using System;
//
using System.Windows.Forms;

namespace SRCPLite_hIVE.Models_CS
{
    class FileManager : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #region ConnectionModels
        //----------

        private TryCatch _TryCatch = new TryCatch();
        private string MesExpErrorForms = string.Empty;

        //----------
        #endregion ConnectionModels

        //------------
        ////----------////---------------------------------------------------------------------// Begin Codes
        //------------

        private static string InputDefaultDataBasePath = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);

        private static string OutputDefaultDataBasePath = Application.StartupPath + "\\DataBases";

        //----------

        public string GetDefaultInDBPath
        {
            get { return InputDefaultDataBasePath; }
        }

        public string GetDefaultOutDBPath
        {
            get { return OutputDefaultDataBasePath; }
        }

    }
}