﻿namespace SRCPLite_hIVE
{
    partial class FrmStartup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmStartup));
            this.toolStripMenu = new System.Windows.Forms.ToolStrip();
            this.btnAboutUS = new System.Windows.Forms.ToolStripButton();
            this.toolStripLogo = new System.Windows.Forms.ToolStrip();
            this.lblProductLogo = new System.Windows.Forms.ToolStripLabel();
            this.lblMainProductName = new System.Windows.Forms.ToolStripLabel();
            this.panelFrmStartup = new System.Windows.Forms.Panel();
            this.tlpStartupForm = new System.Windows.Forms.TableLayoutPanel();
            this.tlpRunCancleFunction = new System.Windows.Forms.TableLayoutPanel();
            this.btnCancleFunction = new System.Windows.Forms.Button();
            this.btnRunFunction = new System.Windows.Forms.Button();
            this.grbNewDbPassword = new System.Windows.Forms.GroupBox();
            this.btnNewDbPassDelete = new System.Windows.Forms.Button();
            this.btnNewDbPassSet = new System.Windows.Forms.Button();
            this.txtNewDbPassword = new System.Windows.Forms.TextBox();
            this.grbDbPassword = new System.Windows.Forms.GroupBox();
            this.btnDbPassDelete = new System.Windows.Forms.Button();
            this.btnDbPassSet = new System.Windows.Forms.Button();
            this.txtDbPassword = new System.Windows.Forms.TextBox();
            this.grbInputDbFile = new System.Windows.Forms.GroupBox();
            this.rdbInDbPathMainSave = new System.Windows.Forms.RadioButton();
            this.rdbInDbPathStartupSave = new System.Windows.Forms.RadioButton();
            this.btnInDbPathLoad = new System.Windows.Forms.Button();
            this.txtShowInputDbPath = new System.Windows.Forms.TextBox();
            this.grbFunctionType = new System.Windows.Forms.GroupBox();
            this.tlpFunctionType = new System.Windows.Forms.TableLayoutPanel();
            this.rdbDbPassChange = new System.Windows.Forms.RadioButton();
            this.rdbDbPassRemove = new System.Windows.Forms.RadioButton();
            this.rdbDbPassSet = new System.Windows.Forms.RadioButton();
            this.toolStripMenu.SuspendLayout();
            this.toolStripLogo.SuspendLayout();
            this.panelFrmStartup.SuspendLayout();
            this.tlpStartupForm.SuspendLayout();
            this.tlpRunCancleFunction.SuspendLayout();
            this.grbNewDbPassword.SuspendLayout();
            this.grbDbPassword.SuspendLayout();
            this.grbInputDbFile.SuspendLayout();
            this.grbFunctionType.SuspendLayout();
            this.tlpFunctionType.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripMenu
            // 
            this.toolStripMenu.BackColor = System.Drawing.Color.LightSeaGreen;
            this.toolStripMenu.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAboutUS});
            this.toolStripMenu.Location = new System.Drawing.Point(0, 0);
            this.toolStripMenu.Name = "toolStripMenu";
            this.toolStripMenu.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStripMenu.Size = new System.Drawing.Size(444, 25);
            this.toolStripMenu.TabIndex = 7;
            this.toolStripMenu.Text = "toolStripMenu";
            // 
            // btnAboutUS
            // 
            this.btnAboutUS.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.btnAboutUS.Image = global::SRCPLite_hIVE.Properties.Resources.btnInfo;
            this.btnAboutUS.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAboutUS.Name = "btnAboutUS";
            this.btnAboutUS.Size = new System.Drawing.Size(68, 22);
            this.btnAboutUS.Text = "درباره ما";
            this.btnAboutUS.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.btnAboutUS.Click += new System.EventHandler(this.btnAboutUS_Click);
            // 
            // toolStripLogo
            // 
            this.toolStripLogo.BackColor = System.Drawing.Color.Turquoise;
            this.toolStripLogo.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStripLogo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblProductLogo,
            this.lblMainProductName});
            this.toolStripLogo.Location = new System.Drawing.Point(0, 25);
            this.toolStripLogo.Name = "toolStripLogo";
            this.toolStripLogo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStripLogo.Size = new System.Drawing.Size(444, 53);
            this.toolStripLogo.TabIndex = 8;
            this.toolStripLogo.Text = "toolStrip2";
            // 
            // lblProductLogo
            // 
            this.lblProductLogo.AutoSize = false;
            this.lblProductLogo.BackgroundImage = global::SRCPLite_hIVE.Properties.Resources.lblLogo;
            this.lblProductLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.lblProductLogo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.lblProductLogo.Font = new System.Drawing.Font("B Nazanin", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblProductLogo.ForeColor = System.Drawing.Color.Red;
            this.lblProductLogo.Name = "lblProductLogo";
            this.lblProductLogo.Size = new System.Drawing.Size(50, 50);
            this.lblProductLogo.Text = "لوگو";
            // 
            // lblMainProductName
            // 
            this.lblMainProductName.Font = new System.Drawing.Font("B Jadid", 13.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lblMainProductName.ForeColor = System.Drawing.Color.Red;
            this.lblMainProductName.Name = "lblMainProductName";
            this.lblMainProductName.Size = new System.Drawing.Size(364, 50);
            this.lblMainProductName.Text = "تنظیم، حذف و تغییر رمز پایگاه‌داده لایت - کندو";
            // 
            // panelFrmStartup
            // 
            this.panelFrmStartup.BackColor = System.Drawing.Color.CornflowerBlue;
            this.panelFrmStartup.Controls.Add(this.tlpStartupForm);
            this.panelFrmStartup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFrmStartup.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.panelFrmStartup.Location = new System.Drawing.Point(0, 78);
            this.panelFrmStartup.Name = "panelFrmStartup";
            this.panelFrmStartup.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.panelFrmStartup.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.panelFrmStartup.Size = new System.Drawing.Size(444, 523);
            this.panelFrmStartup.TabIndex = 9;
            // 
            // tlpStartupForm
            // 
            this.tlpStartupForm.ColumnCount = 1;
            this.tlpStartupForm.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpStartupForm.Controls.Add(this.tlpRunCancleFunction, 0, 5);
            this.tlpStartupForm.Controls.Add(this.grbNewDbPassword, 0, 3);
            this.tlpStartupForm.Controls.Add(this.grbDbPassword, 0, 2);
            this.tlpStartupForm.Controls.Add(this.grbInputDbFile, 0, 1);
            this.tlpStartupForm.Controls.Add(this.grbFunctionType, 0, 0);
            this.tlpStartupForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpStartupForm.Location = new System.Drawing.Point(20, 30);
            this.tlpStartupForm.Name = "tlpStartupForm";
            this.tlpStartupForm.Padding = new System.Windows.Forms.Padding(5);
            this.tlpStartupForm.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tlpStartupForm.RowCount = 6;
            this.tlpStartupForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpStartupForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpStartupForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpStartupForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpStartupForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8F));
            this.tlpStartupForm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12F));
            this.tlpStartupForm.Size = new System.Drawing.Size(404, 473);
            this.tlpStartupForm.TabIndex = 10;
            // 
            // tlpRunCancleFunction
            // 
            this.tlpRunCancleFunction.ColumnCount = 3;
            this.tlpRunCancleFunction.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpRunCancleFunction.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpRunCancleFunction.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpRunCancleFunction.Controls.Add(this.btnCancleFunction, 2, 0);
            this.tlpRunCancleFunction.Controls.Add(this.btnRunFunction, 0, 0);
            this.tlpRunCancleFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpRunCancleFunction.Location = new System.Drawing.Point(10, 415);
            this.tlpRunCancleFunction.Margin = new System.Windows.Forms.Padding(5);
            this.tlpRunCancleFunction.Name = "tlpRunCancleFunction";
            this.tlpRunCancleFunction.RowCount = 1;
            this.tlpRunCancleFunction.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpRunCancleFunction.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tlpRunCancleFunction.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tlpRunCancleFunction.Size = new System.Drawing.Size(384, 48);
            this.tlpRunCancleFunction.TabIndex = 5;
            // 
            // btnCancleFunction
            // 
            this.btnCancleFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancleFunction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancleFunction.Location = new System.Drawing.Point(5, 5);
            this.btnCancleFunction.Margin = new System.Windows.Forms.Padding(5);
            this.btnCancleFunction.Name = "btnCancleFunction";
            this.btnCancleFunction.Padding = new System.Windows.Forms.Padding(3);
            this.btnCancleFunction.Size = new System.Drawing.Size(118, 38);
            this.btnCancleFunction.TabIndex = 3;
            this.btnCancleFunction.Text = "انصراف";
            this.btnCancleFunction.UseVisualStyleBackColor = true;
            this.btnCancleFunction.Click += new System.EventHandler(this.btnCancleFunction_Click);
            // 
            // btnRunFunction
            // 
            this.btnRunFunction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnRunFunction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRunFunction.Location = new System.Drawing.Point(261, 5);
            this.btnRunFunction.Margin = new System.Windows.Forms.Padding(5);
            this.btnRunFunction.Name = "btnRunFunction";
            this.btnRunFunction.Padding = new System.Windows.Forms.Padding(3);
            this.btnRunFunction.Size = new System.Drawing.Size(118, 38);
            this.btnRunFunction.TabIndex = 2;
            this.btnRunFunction.Text = "اجرا";
            this.btnRunFunction.UseVisualStyleBackColor = true;
            this.btnRunFunction.Click += new System.EventHandler(this.btnRunFunction_Click);
            // 
            // grbNewDbPassword
            // 
            this.grbNewDbPassword.BackColor = System.Drawing.Color.Transparent;
            this.grbNewDbPassword.Controls.Add(this.btnNewDbPassDelete);
            this.grbNewDbPassword.Controls.Add(this.btnNewDbPassSet);
            this.grbNewDbPassword.Controls.Add(this.txtNewDbPassword);
            this.grbNewDbPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbNewDbPassword.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbNewDbPassword.Location = new System.Drawing.Point(8, 284);
            this.grbNewDbPassword.Name = "grbNewDbPassword";
            this.grbNewDbPassword.Size = new System.Drawing.Size(388, 86);
            this.grbNewDbPassword.TabIndex = 3;
            this.grbNewDbPassword.TabStop = false;
            this.grbNewDbPassword.Text = "پسورد جدید";
            // 
            // btnNewDbPassDelete
            // 
            this.btnNewDbPassDelete.BackColor = System.Drawing.Color.Crimson;
            this.btnNewDbPassDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewDbPassDelete.Location = new System.Drawing.Point(6, 33);
            this.btnNewDbPassDelete.Name = "btnNewDbPassDelete";
            this.btnNewDbPassDelete.Size = new System.Drawing.Size(60, 23);
            this.btnNewDbPassDelete.TabIndex = 8;
            this.btnNewDbPassDelete.Text = "حذف";
            this.btnNewDbPassDelete.UseVisualStyleBackColor = false;
            this.btnNewDbPassDelete.Click += new System.EventHandler(this.btnNewDbPassDelete_Click);
            // 
            // btnNewDbPassSet
            // 
            this.btnNewDbPassSet.BackColor = System.Drawing.Color.Lime;
            this.btnNewDbPassSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewDbPassSet.Location = new System.Drawing.Point(322, 33);
            this.btnNewDbPassSet.Name = "btnNewDbPassSet";
            this.btnNewDbPassSet.Size = new System.Drawing.Size(60, 23);
            this.btnNewDbPassSet.TabIndex = 7;
            this.btnNewDbPassSet.Text = "چسباندن";
            this.btnNewDbPassSet.UseVisualStyleBackColor = false;
            this.btnNewDbPassSet.Click += new System.EventHandler(this.btnNewDbPassSet_Click);
            // 
            // txtNewDbPassword
            // 
            this.txtNewDbPassword.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtNewDbPassword.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtNewDbPassword.Location = new System.Drawing.Point(3, 61);
            this.txtNewDbPassword.Margin = new System.Windows.Forms.Padding(20, 3, 20, 3);
            this.txtNewDbPassword.Name = "txtNewDbPassword";
            this.txtNewDbPassword.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtNewDbPassword.Size = new System.Drawing.Size(382, 22);
            this.txtNewDbPassword.TabIndex = 6;
            // 
            // grbDbPassword
            // 
            this.grbDbPassword.BackColor = System.Drawing.Color.Transparent;
            this.grbDbPassword.Controls.Add(this.btnDbPassDelete);
            this.grbDbPassword.Controls.Add(this.btnDbPassSet);
            this.grbDbPassword.Controls.Add(this.txtDbPassword);
            this.grbDbPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbDbPassword.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbDbPassword.Location = new System.Drawing.Point(8, 192);
            this.grbDbPassword.Name = "grbDbPassword";
            this.grbDbPassword.Size = new System.Drawing.Size(388, 86);
            this.grbDbPassword.TabIndex = 2;
            this.grbDbPassword.TabStop = false;
            this.grbDbPassword.Text = "پسورد";
            // 
            // btnDbPassDelete
            // 
            this.btnDbPassDelete.BackColor = System.Drawing.Color.Crimson;
            this.btnDbPassDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDbPassDelete.Location = new System.Drawing.Point(6, 30);
            this.btnDbPassDelete.Name = "btnDbPassDelete";
            this.btnDbPassDelete.Size = new System.Drawing.Size(60, 23);
            this.btnDbPassDelete.TabIndex = 7;
            this.btnDbPassDelete.Text = "حذف";
            this.btnDbPassDelete.UseVisualStyleBackColor = false;
            this.btnDbPassDelete.Click += new System.EventHandler(this.btnDbPassDelete_Click);
            // 
            // btnDbPassSet
            // 
            this.btnDbPassSet.BackColor = System.Drawing.Color.Lime;
            this.btnDbPassSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDbPassSet.Location = new System.Drawing.Point(322, 30);
            this.btnDbPassSet.Name = "btnDbPassSet";
            this.btnDbPassSet.Size = new System.Drawing.Size(60, 23);
            this.btnDbPassSet.TabIndex = 6;
            this.btnDbPassSet.Text = "چسباندن";
            this.btnDbPassSet.UseVisualStyleBackColor = false;
            this.btnDbPassSet.Click += new System.EventHandler(this.btnDbPassSet_Click);
            // 
            // txtDbPassword
            // 
            this.txtDbPassword.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtDbPassword.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtDbPassword.Location = new System.Drawing.Point(3, 61);
            this.txtDbPassword.Margin = new System.Windows.Forms.Padding(20, 3, 20, 3);
            this.txtDbPassword.Name = "txtDbPassword";
            this.txtDbPassword.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDbPassword.Size = new System.Drawing.Size(382, 22);
            this.txtDbPassword.TabIndex = 5;
            // 
            // grbInputDbFile
            // 
            this.grbInputDbFile.BackColor = System.Drawing.Color.Transparent;
            this.grbInputDbFile.Controls.Add(this.rdbInDbPathMainSave);
            this.grbInputDbFile.Controls.Add(this.rdbInDbPathStartupSave);
            this.grbInputDbFile.Controls.Add(this.btnInDbPathLoad);
            this.grbInputDbFile.Controls.Add(this.txtShowInputDbPath);
            this.grbInputDbFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbInputDbFile.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbInputDbFile.Location = new System.Drawing.Point(8, 100);
            this.grbInputDbFile.Name = "grbInputDbFile";
            this.grbInputDbFile.Size = new System.Drawing.Size(388, 86);
            this.grbInputDbFile.TabIndex = 1;
            this.grbInputDbFile.TabStop = false;
            this.grbInputDbFile.Text = "فایل ورودی";
            // 
            // rdbInDbPathMainSave
            // 
            this.rdbInDbPathMainSave.AutoSize = true;
            this.rdbInDbPathMainSave.Checked = true;
            this.rdbInDbPathMainSave.Location = new System.Drawing.Point(131, 21);
            this.rdbInDbPathMainSave.Name = "rdbInDbPathMainSave";
            this.rdbInDbPathMainSave.Size = new System.Drawing.Size(109, 19);
            this.rdbInDbPathMainSave.TabIndex = 8;
            this.rdbInDbPathMainSave.TabStop = true;
            this.rdbInDbPathMainSave.Text = "ذخیره در مسیر فایل";
            this.rdbInDbPathMainSave.UseVisualStyleBackColor = true;
            // 
            // rdbInDbPathStartupSave
            // 
            this.rdbInDbPathStartupSave.AutoSize = true;
            this.rdbInDbPathStartupSave.Location = new System.Drawing.Point(6, 21);
            this.rdbInDbPathStartupSave.Name = "rdbInDbPathStartupSave";
            this.rdbInDbPathStartupSave.Size = new System.Drawing.Size(119, 19);
            this.rdbInDbPathStartupSave.TabIndex = 6;
            this.rdbInDbPathStartupSave.Text = "ذخیره در مسیر برنامه";
            this.rdbInDbPathStartupSave.UseVisualStyleBackColor = true;
            // 
            // btnInDbPathLoad
            // 
            this.btnInDbPathLoad.BackColor = System.Drawing.Color.Aqua;
            this.btnInDbPathLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInDbPathLoad.Location = new System.Drawing.Point(292, 30);
            this.btnInDbPathLoad.Name = "btnInDbPathLoad";
            this.btnInDbPathLoad.Size = new System.Drawing.Size(90, 23);
            this.btnInDbPathLoad.TabIndex = 5;
            this.btnInDbPathLoad.Text = "بارگیری مسیر";
            this.btnInDbPathLoad.UseVisualStyleBackColor = false;
            this.btnInDbPathLoad.Click += new System.EventHandler(this.btnInDbPathLoad_Click);
            // 
            // txtShowInputDbPath
            // 
            this.txtShowInputDbPath.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtShowInputDbPath.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtShowInputDbPath.Location = new System.Drawing.Point(3, 61);
            this.txtShowInputDbPath.Margin = new System.Windows.Forms.Padding(20, 3, 20, 3);
            this.txtShowInputDbPath.Name = "txtShowInputDbPath";
            this.txtShowInputDbPath.ReadOnly = true;
            this.txtShowInputDbPath.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtShowInputDbPath.Size = new System.Drawing.Size(382, 22);
            this.txtShowInputDbPath.TabIndex = 4;
            // 
            // grbFunctionType
            // 
            this.grbFunctionType.BackColor = System.Drawing.Color.Transparent;
            this.grbFunctionType.Controls.Add(this.tlpFunctionType);
            this.grbFunctionType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbFunctionType.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.grbFunctionType.Location = new System.Drawing.Point(8, 8);
            this.grbFunctionType.Name = "grbFunctionType";
            this.grbFunctionType.Size = new System.Drawing.Size(388, 86);
            this.grbFunctionType.TabIndex = 0;
            this.grbFunctionType.TabStop = false;
            this.grbFunctionType.Text = "نوع عملیات";
            // 
            // tlpFunctionType
            // 
            this.tlpFunctionType.ColumnCount = 3;
            this.tlpFunctionType.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpFunctionType.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpFunctionType.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpFunctionType.Controls.Add(this.rdbDbPassChange, 2, 0);
            this.tlpFunctionType.Controls.Add(this.rdbDbPassRemove, 1, 0);
            this.tlpFunctionType.Controls.Add(this.rdbDbPassSet, 0, 0);
            this.tlpFunctionType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpFunctionType.Location = new System.Drawing.Point(3, 18);
            this.tlpFunctionType.Name = "tlpFunctionType";
            this.tlpFunctionType.RowCount = 1;
            this.tlpFunctionType.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpFunctionType.Size = new System.Drawing.Size(382, 65);
            this.tlpFunctionType.TabIndex = 0;
            // 
            // rdbDbPassChange
            // 
            this.rdbDbPassChange.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rdbDbPassChange.AutoSize = true;
            this.rdbDbPassChange.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.rdbDbPassChange.Location = new System.Drawing.Point(3, 16);
            this.rdbDbPassChange.Name = "rdbDbPassChange";
            this.rdbDbPassChange.Size = new System.Drawing.Size(122, 32);
            this.rdbDbPassChange.TabIndex = 2;
            this.rdbDbPassChange.Text = "تغییر پسورد";
            this.rdbDbPassChange.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.rdbDbPassChange.UseVisualStyleBackColor = true;
            this.rdbDbPassChange.CheckedChanged += new System.EventHandler(this.rdbDbPassChange_CheckedChanged);
            // 
            // rdbDbPassRemove
            // 
            this.rdbDbPassRemove.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rdbDbPassRemove.AutoSize = true;
            this.rdbDbPassRemove.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.rdbDbPassRemove.Location = new System.Drawing.Point(131, 16);
            this.rdbDbPassRemove.Name = "rdbDbPassRemove";
            this.rdbDbPassRemove.Size = new System.Drawing.Size(121, 32);
            this.rdbDbPassRemove.TabIndex = 1;
            this.rdbDbPassRemove.Text = "حذف پسورد";
            this.rdbDbPassRemove.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.rdbDbPassRemove.UseVisualStyleBackColor = true;
            this.rdbDbPassRemove.CheckedChanged += new System.EventHandler(this.rdbDbPassRemove_CheckedChanged);
            // 
            // rdbDbPassSet
            // 
            this.rdbDbPassSet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.rdbDbPassSet.AutoSize = true;
            this.rdbDbPassSet.CheckAlign = System.Drawing.ContentAlignment.TopCenter;
            this.rdbDbPassSet.Location = new System.Drawing.Point(258, 16);
            this.rdbDbPassSet.Name = "rdbDbPassSet";
            this.rdbDbPassSet.Size = new System.Drawing.Size(121, 32);
            this.rdbDbPassSet.TabIndex = 0;
            this.rdbDbPassSet.Text = "تنظیم پسورد";
            this.rdbDbPassSet.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.rdbDbPassSet.UseVisualStyleBackColor = true;
            this.rdbDbPassSet.CheckedChanged += new System.EventHandler(this.rdbDbPassSet_CheckedChanged);
            // 
            // FrmStartup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(444, 601);
            this.Controls.Add(this.panelFrmStartup);
            this.Controls.Add(this.toolStripLogo);
            this.Controls.Add(this.toolStripMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmStartup";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SRCPLite - hIVE";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmStartup_FormClosing);
            this.Load += new System.EventHandler(this.FrmStartup_Load);
            this.toolStripMenu.ResumeLayout(false);
            this.toolStripMenu.PerformLayout();
            this.toolStripLogo.ResumeLayout(false);
            this.toolStripLogo.PerformLayout();
            this.panelFrmStartup.ResumeLayout(false);
            this.tlpStartupForm.ResumeLayout(false);
            this.tlpRunCancleFunction.ResumeLayout(false);
            this.grbNewDbPassword.ResumeLayout(false);
            this.grbNewDbPassword.PerformLayout();
            this.grbDbPassword.ResumeLayout(false);
            this.grbDbPassword.PerformLayout();
            this.grbInputDbFile.ResumeLayout(false);
            this.grbInputDbFile.PerformLayout();
            this.grbFunctionType.ResumeLayout(false);
            this.tlpFunctionType.ResumeLayout(false);
            this.tlpFunctionType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripMenu;
        private System.Windows.Forms.ToolStripButton btnAboutUS;
        private System.Windows.Forms.ToolStrip toolStripLogo;
        private System.Windows.Forms.ToolStripLabel lblProductLogo;
        private System.Windows.Forms.ToolStripLabel lblMainProductName;
        private System.Windows.Forms.Panel panelFrmStartup;
        private System.Windows.Forms.TableLayoutPanel tlpStartupForm;
        private System.Windows.Forms.GroupBox grbNewDbPassword;
        private System.Windows.Forms.GroupBox grbDbPassword;
        private System.Windows.Forms.GroupBox grbInputDbFile;
        private System.Windows.Forms.GroupBox grbFunctionType;
        private System.Windows.Forms.TableLayoutPanel tlpFunctionType;
        private System.Windows.Forms.RadioButton rdbDbPassChange;
        private System.Windows.Forms.RadioButton rdbDbPassRemove;
        private System.Windows.Forms.RadioButton rdbDbPassSet;
        private System.Windows.Forms.TextBox txtShowInputDbPath;
        private System.Windows.Forms.Button btnInDbPathLoad;
        private System.Windows.Forms.RadioButton rdbInDbPathMainSave;
        private System.Windows.Forms.RadioButton rdbInDbPathStartupSave;
        private System.Windows.Forms.Button btnDbPassDelete;
        private System.Windows.Forms.Button btnDbPassSet;
        private System.Windows.Forms.TextBox txtDbPassword;
        private System.Windows.Forms.Button btnNewDbPassDelete;
        private System.Windows.Forms.Button btnNewDbPassSet;
        private System.Windows.Forms.TextBox txtNewDbPassword;
        private System.Windows.Forms.TableLayoutPanel tlpRunCancleFunction;
        private System.Windows.Forms.Button btnCancleFunction;
        private System.Windows.Forms.Button btnRunFunction;
    }
}

